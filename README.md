This is a DEMO of basic tests both in Python and in C++ (Using gtest).
for more info refer to [bpinaya.com](https://www.bpinaya.com).

to run the tests:

```bash

catkin_make run_tests && catkin_make test

```
A dedicated tutorial coming soon.

All the best,
Ben
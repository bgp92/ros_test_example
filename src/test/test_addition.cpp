#include "calculator.h"
#include <gtest/gtest.h>

TEST(TestSuite, addition)
{
  const double ret = addition(2,3);
  ASSERT_EQ(5, ret);
}

TEST(TestSuite, addition_decimal)
{
  const double ret = addition(1.9,1.1);
  ASSERT_EQ(3.0, ret);
}

TEST(TestSuite, addition_negative)
{
  const double ret = addition(3,-1);
  ASSERT_EQ(2, ret);
}

// Run all the tests that were declared with TEST()
int main(int argc, char **argv){
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
